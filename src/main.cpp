#include <Arduino.h>
#include <WiFi.h>
#include <NTPClient.h>

enum {WIFI_NOT_CONNECTED, WIFI_CONNECTED} WIFIState;

int ledPin = 22;
String ssid = "the-lair";
String password = "El Sapo est le meilleur !";

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
String formattedDate;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  WIFIState = WIFI_NOT_CONNECTED;
  bool found = false;
  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; i++){
    if (ssid == WiFi.SSID(i)) {
        found = true;
    }
  }
  if (found) {
    WiFi.begin(ssid.c_str(), password.c_str());
    Serial.println("En attente de connexion...");
    while(WiFi.status() != WL_CONNECTED);
    WIFIState = WIFI_CONNECTED;
    timeClient.begin();
    timeClient.setTimeOffset(3600);
  }
  else {
    Serial.println("Le point WiFi spécifié n'a pas été trouvé...");
  }
}

void loop() {
  String timeStamp;
  int splitT;
  switch (WIFIState)
  {
  case WIFI_CONNECTED:
    while(!timeClient.update()) {
      timeClient.forceUpdate();
    }
    formattedDate = timeClient.getFormattedDate();
    splitT = formattedDate.indexOf("T");
    timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
    Serial.println(timeStamp);
    break;
  case WIFI_NOT_CONNECTED:
    Serial.println("Le WiFi n'est pas connecté...");
    break;
  default:
    Serial.println("Ce cas ne doit jamais ce produire");
    break;
  }
  delay(1000);
}