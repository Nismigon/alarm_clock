# Alarm Clock

The aim of this project is to create an alarm clock with speakers and Wi-Fi connectivity. In order to use that, the alarm will be connected with a Raspberry Pi to set clocks, to set alarms and to stream musics.

The alarm clock will be composed of :

- 2 speakers (8 Ohm, 3 Watt) [Amazon](https://www.amazon.fr/gp/product/B07FT9CFY4/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)
- Points Matrix x4 [Amazon](https://www.amazon.fr/gp/product/B07J2QTK57/ref=ppx_yo_dt_b_asin_title_o01_s01?ie=UTF8&psc=1)
- LOLIN32 (ESP32 board) [Amazon](https://www.amazon.fr/gp/product/B086V1P4BL/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1)
